variable "resource_group_name" {
  description = "Nom du groupe de ressources"
  default     = "OCC-ASD-KHALID-KUB-PROD"
}

variable "location" {
  description = "Emplacement du groupe de ressources"
  default     = "eastus"
}

variable "aks_cluster_name" {
  description = "Nom du cluster AKS"
  default     = "myAKSCluster"
}

variable "node_count" {
  description = "Nombre de nœuds dans le cluster AKS"
  default     = 3
}
