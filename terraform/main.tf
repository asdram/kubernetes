# Configurer le fournisseur Azure
provider "azurerm" {
  features {}
}

# Créer un groupe de ressources
resource "azurerm_resource_group" "main" {
  name     = var.resource_group_name
  location = var.location
}

# Créer un réseau virtuel
resource "azurerm_virtual_network" "main" {
  name                = "myVNet"
  address_space       = ["10.0.0.0/16"]
  location            = var.location
  resource_group_name = var.resource_group_name
}

# Créer un sous-réseau
resource "azurerm_subnet" "main" {
  name                 = "mySubnet"
  resource_group_name  = var.resource_group_name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefixes     = ["10.0.1.0/24"]
}

# Créer un cluster AKS
resource "azurerm_kubernetes_cluster" "aks" {
  name                = var.aks_cluster_name
  location            = var.location
  resource_group_name = var.resource_group_name
  dns_prefix          = "aks"

  default_node_pool {
    name       = "default"
    node_count = var.node_count
    vm_size    = "Standard_DS2_v2"
    vnet_subnet_id = azurerm_subnet.main.id
  }

  network_profile {
    network_plugin      = "azure"
    service_cidr        = "10.2.0.0/16"
    dns_service_ip      = "10.2.0.10"
    docker_bridge_cidr  = "172.17.0.1/16"
  }

  identity {
    type = "SystemAssigned"
  }

  tags = {
    environment = "development"
  }
}

# Créer un compte de stockage
resource "azurerm_storage_account" "main" {
  name                     = "kubctlstora"
  resource_group_name      = var.resource_group_name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

# Créer un conteneur de stockage
resource "azurerm_storage_container" "main" {
  name                  = "mycontainer"
  storage_account_name  = azurerm_storage_account.main.name
  container_access_type = "private"
}
